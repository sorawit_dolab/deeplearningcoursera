# Deep Learning Specialization on Coursera
### [link](https://www.coursera.org/specializations/deep-learning)
Instructor: [Andrew Ng, DeepLearning.ai]()

These are the video links to the courses on Youtube (note the C1W1L01 means Course 1, Week 1, Lecture 1). The links to the homeworks are also given below. 

## Course 1. [Neural Networks and Deep Learning](https://www.youtube.com/watch?v=CS4cs9xVecg&list=PLkDaE6sCZn6Ec-XTbcX1uRg2_u4xOEky0)
 
1. Week1 - [Introduction to deep learning]
    - No homework :)
2. Week2 - [Neural Networks Basics]
    - [Week 2 - PA 1 - Logistic Regression with a Neural Network mindset](http://git.gurusquare.com/sorawit/Deep-Learning-Coursera/blob/master/Neural%20Networks%20and%20Deep%20Learning/Logistic%20Regression%20with%20a%20Neural%20Network%20mindset.ipynb)
3. Week3 - [Shallow neural networks]
    - [Week 3 - PA 2 - Planar data classification with one hidden layer](http://git.gurusquare.com/sorawit/Deep-Learning-Coursera/blob/master/Neural%20Networks%20and%20Deep%20Learning/Planar%20data%20classification%20with%20one%20hidden%20layer.ipynb)
4. Week4 - [Deep Neural Networks]
    - [Week 4 - PA 3 - Building your Deep Neural Network: Step by Step](http://git.gurusquare.com/sorawit/Deep-Learning-Coursera/blob/master/Neural%20Networks%20and%20Deep%20Learning/Building%20your%20Deep%20Neural%20Network%20-%20Step%20by%20Step.ipynb)
    - [Week 4 - PA 4 - Deep Neural Network for Image Classification: Application](http://git.gurusquare.com/sorawit/Deep-Learning-Coursera/blob/master/Neural%20Networks%20and%20Deep%20Learning/Deep%20Neural%20Network%20-%20Application.ipynb)

## Course 2. [Improving Deep Neural Networks Hyperparameter tuning, Regularization and Optimization](https://www.youtube.com/watch?v=1waHlpKiNyY&list=PLkDaE6sCZn6Hn0vK8co82zjQtt3T2Nkqc)

1. Week1 - [Practical aspects of Deep Learning] - Setting up your Machine Learning Application - Regularizing your neural network - Setting up your optimization problem
    - [Week 1 - PA 1 - Initialization](http://git.gurusquare.com/sorawit/Deep-Learning-Coursera/blob/master/Improving%20Deep%20Neural%20Networks%20Hyperparameter%20tuning,%20Regularization%20and%20Optimization/Initialization.ipynb)
    - [Week 1 - PA 2 - Regularization](http://git.gurusquare.com/sorawit/Deep-Learning-Coursera/blob/master/Improving%20Deep%20Neural%20Networks%20Hyperparameter%20tuning,%20Regularization%20and%20Optimization/Regularization.ipynb)
    - [Week 1 - PA 3 - Gradient Checking](http://git.gurusquare.com/sorawit/Deep-Learning-Coursera/blob/master/Improving%20Deep%20Neural%20Networks%20Hyperparameter%20tuning,%20Regularization%20and%20Optimization/Gradient%20Checking.ipynb)
2. Week2 - [Optimization algorithms]
    - [Week 2 - PA 4 - Optimization Methods](http://git.gurusquare.com/sorawit/Deep-Learning-Coursera/blob/master/Improving%20Deep%20Neural%20Networks%20Hyperparameter%20tuning,%20Regularization%20and%20Optimization/Optimization%20methods.ipynb)
3. Week3 - [Hyperparameter tuning, Batch Normalization and Programming Frameworks]
    - [Week 3 - PA 5 - TensorFlow Tutorial](http://git.gurusquare.com/sorawit/Deep-Learning-Coursera/blob/master/Improving%20Deep%20Neural%20Networks%20Hyperparameter%20tuning,%20Regularization%20and%20Optimization/Tensorflow%20Tutorial.ipynb)

## Course 3. [Structuring Machine Learning Projects](https://www.youtube.com/watch?v=dFX8k1kXhOw&list=PLkDaE6sCZn6E7jZ9sN_xHwSHOdjUxUW_b)  
There is no PA for this course. But this course comes with very interesting case study quizzes.

1. Week1 - [Introduction to ML Strategy]
         - Setting up your goal
         - Comparing to human-level performance
2. Week2 - [ML Strategy (2)]
         - Error Analysis
         - Mismatched training and dev/test set
         - Learning from multiple tasks
         - End-to-end deep learning
         
## Course 4. [Convolutional Neural Networks](https://www.youtube.com/watch?v=ArPaAX_PhIs&list=PLkDaE6sCZn6Gl29AoE31iwdVwSG-KnDzF)
 
 1. Week1 - [Foundations of Convolutional Neural Networks]
    - [Week 1 - PA 1 - Convolutional Model: step by step](http://git.gurusquare.com/sorawit/Deep-Learning-Coursera/blob/master/Convolutional%20Neural%20Networks/Week1/Convolution%20model%20-%20Step%20by%20Step%20-%20v2.ipynb)
    - [Week 1 - PA 2 - Convolutional Model: application](http://git.gurusquare.com/sorawit/Deep-Learning-Coursera/blob/master/Convolutional%20Neural%20Networks/Week1/Convolution%20model%20-%20Application%20-%20v1.ipynb)
 2. Week2 - [Deep convolutional models: case studies] - Papers for read:  [ImageNet Classification with Deep Convolutional
Neural Networks](https://papers.nips.cc/paper/4824-imagenet-classification-with-deep-convolutional-neural-networks.pdf), [Very Deep Convolutional Networks For Large-Scale Image Recognition](https://arxiv.org/pdf/1409.1556.pdf)
    - [Week 2 - PA 1 - Keras - Tutorial - Happy House]()
    - [Week 2 - PA 2 - Residual Networks](http://git.gurusquare.com/sorawit/Deep-Learning-Coursera/blob/master/Convolutional%20Neural%20Networks/Week2/ResNets/Residual%20Networks%20-%20v1.ipynb)
 3. Week3 - [Object detection] - Papers for read: [You Only Look Once:
Unified, Real-Time Object Detection](https://arxiv.org/pdf/1506.02640.pdf), [YOLO](https://arxiv.org/pdf/1612.08242.pdf)
 4. Week4 - [Special applications: Face recognition & Neural style transfer](https://github.com/enggen/Deep-Learning-Coursera/tree/master/Convolutional%20Neural%20Networks/Week4) - Papers for read: [DeepFace](https://www.cs.toronto.edu/~ranzato/publications/taigman_cvpr14.pdf), [FaceNet](https://www.cv-foundation.org/openaccess/content_cvpr_2015/papers/Schroff_FaceNet_A_Unified_2015_CVPR_paper.pdf)
 
## Course 5. [Sequence Models](https://www.youtube.com/watch?v=Q8ys8YnDRXM&list=PL1w8k37X_6L_s4ncq-swTBvKDWnRSrinI&index=1)
 1. Week1 - [Recurrent Neural Networks]
    - [Week 1 - PA 1 - Building a Recurrent Neural Network - Step by Step](http://git.gurusquare.com/sorawit/Deep-Learning-Coursera/blob/master/Sequence%20Models/Week1/Building%20a%20Recurrent%20Neural%20Network%20-%20Step%20by%20Step/Building%20a%20Recurrent%20Neural%20Network%20-%20Step%20by%20Step%20-%20v1.ipynb)
    - [Week 1 - PA 2 - Character level language model - Dinosaurus land](http://git.gurusquare.com/sorawit/Deep-Learning-Coursera/blob/master/Sequence%20Models/Week1/Dinosaur%20Island%20--%20Character-level%20language%20model/Dinosaurus%20Island%20--%20Character%20level%20language%20model%20final%20-%20v3.ipynb)
 2. Week2 - [Natural Language Processing & Word Embeddings]
 3. Week3 - [Sequence models & Attention mechanism]
 
<p align="center"> *************************************************************************************************************************************
</p>
